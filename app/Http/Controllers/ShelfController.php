<?php

namespace App\Http\Controllers;

use App\Models\Shelf;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\RateLimiter;


class ShelfController extends Controller
{
    public function showAllShelfs()
    {
        return response()->json(Shelf::all());
    }

    public function showOneShelf($id)
    {
        return response()->json(Shelf::find($id));
    }

    public function create(Request $request)
    {
        $actionKey = 'search_service';
        $threshold = 10;
        try {
            if (RateLimiter::tooManyAttempts($actionKey, $threshold)) {
                // Exceeded the maximum number of failed attempts.
                return $this->failOrFallback();
            }

            $check = Http::get('http://localhost:8000/api/books/' . $request->book_id);

            if (!empty($check->json())) {

                $shelf = new Shelf;
                $shelf->title = $request->title;
                $shelf->book_id = $request->book_id;
                $shelf->save();

                Http::get('http://localhost:8000/api/books/' . $request->book_id . '/add_shelf');

                return response()->json($shelf, 201);

            } else {

                return response()->json('book dont exit', 404);

            }


        } catch(\Exception $e) {
            RateLimiter::hit($actionKey, Carbon::now()->addMinutes(15));

            return $e->getMessage();

        }



    }

    public function update($id, Request $request)
    {
        $shelf = Shelf::findOrFail($id);
        $shelf->title = $request->title;

        return response()->json($shelf, 200);
    }

    public function delete($id)
    {
        Shelf::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
