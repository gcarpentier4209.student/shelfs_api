<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Shelf API';
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('shelfs',  ['uses' => 'ShelfController@showAllShelfs']);

    $router->get('shelfs/{id}', ['uses' => 'ShelfController@showOneShelf']);

    $router->post('shelfs', ['uses' => 'ShelfController@create']);

    $router->delete('shelfs/{id}', ['uses' => 'ShelfController@delete']);

    $router->put('shelfs/{id}', ['uses' => 'ShelfController@update']);
});
